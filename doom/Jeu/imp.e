note
	description: "Classe {IMP} g�re les fonctions des Imps."
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "4.0"

class
	IMP

inherit

	ENNEMI

	GAME_RANDOM_SHARED

create
	make

feature -- Initialisation

	make (a_renderer: GAME_RENDERER)
			-- Initialisation de `Current' qui utilise `a_renderer'
		do
			make_deplacable (a_renderer, "imp.png")
			random.generate_new_random
			x := random.last_random_integer_between (400, 900)
			y := random.last_random_integer_between (200, 360)
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
