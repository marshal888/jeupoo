note
	description: "[
		Gestion des fonctions du Doomguy
	]"
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "4.0"

class
	DOOMGUY

inherit

	DEPLACABLE
		rename
			make as make_deplacable
		redefine
			update
		end

create
	make

feature {NONE}

	make (a_renderer: GAME_RENDERER)
			-- Initialisation of `Current' � utiliser avec `a_renderer'

		do
			make_deplacable (a_renderer, "maryo.png")
			create audio_fire.make ("shotgun_sound.wav")
			health_points :=100
			movement_delta_horizontal := 5
			x := 200
			y := 375
		ensure
			valid_delta: movement_delta_horizontal=5
			valid_x: x=200
			valid_y: y=375
		end

feature

	fire
			-- Fait jouer le son de tire quand 'Current' tire
		require
			No_Error: not has_error
		do
			audio_fire.play_sound (0)
		end

	jump (a_timestamp: NATURAL_32)
			-- Fait sauter le 'Current' selon le 'a_timestamp'

		require
			No_Error: not has_error
		do
			jumping := true
			delta_jumping := a_timestamp
			go_up (a_timestamp)
		end

	jumping: BOOLEAN
			-- Est en train de sauter

	delta_jumping: NATURAL_32
			-- Le temps Delta entre chaque saut de 'Current'

	audio_fire: AUDIO
			-- Son qui se produit quand 'Current' tire

	health_points: INTEGER assign set_health_points
			-- Points de vie du 'Current'

	set_health_points (a_point: INTEGER)
			-- Assigne la valeur de `health_points' avec `a_point' en additionnant 'a_point' a 'health_points'
		do
			health_points :=  health_points + a_point
		end

	update (a_timestamp: NATURAL_32)
			-- <precursor>
		do
			if jumping then
				if a_timestamp - delta_jumping > 300 and going_up then
					stop_up
					go_down (a_timestamp)
				end
				if y > 375 and going_down then
					y := 375
					stop_down
					jumping := false
				end
			end
			precursor (a_timestamp)
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
