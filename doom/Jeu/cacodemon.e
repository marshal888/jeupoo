note
	description: "Classe {CACODEMON} g�re les fonctions des Cacodemons."
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "4.0"

class
	CACODEMON

inherit

	ENNEMI

	GAME_RANDOM_SHARED

create
	make

feature

	make (a_renderer: GAME_RENDERER)
			-- Initialisation de `Current' � utiliser avec `a_renderer'
		do
			make_deplacable (a_renderer, "cacodemon.png")
			random.generate_new_random
			x := random.last_random_integer_between (400, 900)
			y := random.last_random_integer_between (200, 360)
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
