note
	description: "G�re ce qui se passe dans le thread"
	auteur: "Samuel Fortier"
	date: "Dim, 07 Juin 2020 03:08:38 "
	revision: "2.0"

class
	APP_THREAD

inherit

	THREAD
		rename
			make as make_thread
		end

	AUDIO
		rename
			make as make_audio
		end

create
	make

feature {NONE} -- Initialisation

	make (a_procedure_quitter: ROUTINE)
			-- Initialisation de `Current' utilisant `a_procedure_quitter' comme `procedure_quitter'
		do
			make_thread
			make_audio
			procedure_quitter := a_procedure_quitter
		end

feature {NONE} -- Impl�mentation

	execute
			-- l'ex�cution du thread
		local
			quit: BOOLEAN
		do
			from
				quit := false
			until
				quit
			loop
				create audio.make ("sound.wav")
			end
		end

	procedure_quitter: ROUTINE
			-- �v�nement pour terminer `Current'

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
