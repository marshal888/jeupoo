note
	description: "G�re les fonctions des objets qui sont d�placables"
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "2.0"

deferred class
	DEPLACABLE

inherit

	AFFICHAGE
		rename
			is_mirror as facing_left
		redefine
			make
		end

feature

	make (a_renderer: GAME_RENDERER; a_file_name: STRING)
			-- <precursor>
		do
			precursor (a_renderer, a_file_name)
			sub_image_width := width // 3
			initialize_animation_coordinate
		end

	update (a_timestamp: NATURAL_32)
			-- Met � jour la surface d�pendamment du  `a_timestamp'.

		require
			No_Error: not has_error
		local
			l_coordinate: TUPLE [x, y: INTEGER]
			l_delta_time: NATURAL_32
		do
			if going_left or going_right then
				l_coordinate := animation_coordinates.at ((((a_timestamp // animation_delta_horizontal) \\ animation_coordinates.count.to_natural_32) + 1).to_integer_32)
				sub_image_x := l_coordinate.x
				sub_image_y := l_coordinate.y
				l_delta_time := a_timestamp - old_timestamp_horizontal
				if l_delta_time // movement_delta_horizontal > 0 then
					if going_right then
						facing_left := False
						x := x + (l_delta_time // movement_delta_horizontal).to_integer_32
					elseif going_left then
						facing_left := True
						x := x - (l_delta_time // movement_delta_horizontal).to_integer_32
					end
					old_timestamp_horizontal := old_timestamp_horizontal + (l_delta_time // movement_delta_horizontal) * movement_delta_horizontal
				end
			end
			if going_up or going_down then
				l_coordinate := animation_coordinates.at ((((a_timestamp // animation_delta_vertical) \\ animation_coordinates.count.to_natural_32) + 1).to_integer_32)
				l_delta_time := a_timestamp - old_timestamp_vertical
				if l_delta_time // movement_delta_vertical > 0 then
					if going_up then
						y := y - (l_delta_time // movement_delta_vertical).to_integer_32
					else
						y := y + (l_delta_time // movement_delta_vertical).to_integer_32
					end
				end
			end
		end

	initialize_animation_coordinate
			-- Cr�ation de `animation_coordinates'
		do
			create {ARRAYED_LIST [TUPLE [x, y: INTEGER]]} animation_coordinates.make (4)
			animation_coordinates.extend ([width // 3, 0])
			animation_coordinates.extend ([0, 0])
			animation_coordinates.extend ([(width // 3) * 2, 0])
			animation_coordinates.extend ([0, 0])
			sub_image_x := animation_coordinates.first.x -- Met l'image immobile
			sub_image_y := animation_coordinates.first.y -- Met l'image immobile
		ensure
			valid_sub_x: sub_image_x = animation_coordinates.first.x
			valid_sub_y: sub_image_y = animation_coordinates.first.y
		end

	go_left (a_timestamp: NATURAL_32)
			-- Fait que le `Current' commence � bouger � gauche
		do
			old_timestamp_horizontal := a_timestamp
			going_left := True
		end

	go_right (a_timestamp: NATURAL_32)
			-- Fait que le `Current' commence � bouger � droite
		do
			old_timestamp_horizontal := a_timestamp
			going_right := True
		end

	go_up (a_timestamp: NATURAL_32)
			-- Fait que le `Current' commence � bouger vers le haut
		do
			if not going_up then
				old_timestamp_vertical := a_timestamp
			end
			going_up := True
		end

	go_down (a_timestamp: NATURAL_32)
			-- Fait que le `Current' commence � bouger vers le haut
		do
			if not going_down then
				old_timestamp_vertical := a_timestamp
			end
			going_down := True
		end

	stop_left
			-- Fait que le  `Current' arr�te de bouger vers la gauche
		do
			going_left := False
			if not going_right then
				sub_image_x := animation_coordinates.first.x
				sub_image_y := animation_coordinates.first.y
			end
		end

	stop_right
			-- Fait que le  `Current' arr�te de bouger vers la droite
		do
			going_right := False
			if not going_left then
				sub_image_x := animation_coordinates.first.x
				sub_image_y := animation_coordinates.first.y
			end
		end

	stop_up
			-- Fait que le  `Current' arr�te de bouger vers le haut
		do
			going_up := False
		end

	stop_down
			-- Fait que le  `Current' arr�te de bouger vers le bas
		do
			going_down := False
		end

	facing_up: BOOLEAN
			-- Est-ce que 'Current' fait face vers le haut

	going_left: BOOLEAN
			-- Est-ce que 'Current' bouge vers la gauche

	going_down: BOOLEAN
			-- Est-ce que 'Current' bouge vers le bas

	going_right: BOOLEAN
			-- Est-ce que 'Current' bouge vers la droite

	going_up: BOOLEAN
			-- Est-ce que 'Current' bouge vers le haut

feature {NONE} -- implementation

	animation_coordinates: LIST [TUPLE [x, y: INTEGER]]
			-- Toute les coordon�es de la portion des images dans 'surface'

	old_timestamp_horizontal: NATURAL_32
			-- Quand est-ce que le dernier mouvement horizontal a eu lieu (consid�rant `movement_delta_horizontal')

	old_timestamp_vertical: NATURAL_32
			-- Quand le dernier mouvement vertical a eu lieu

feature {NONE} -- constantes

	movement_delta_horizontal: NATURAL_32
			-- Le temps Delta entre chaque mouvement horizontal de `Current'

	movement_delta_vertical: NATURAL_32 = 15
			-- Le temps Delta entre chaque mouvement vertical de `Current'

	animation_delta_horizontal: NATURAL_32 = 100
			-- Le temps Delta entre chaque animation horizontale de `Current'

	animation_delta_vertical: NATURAL_32 = 1000
			-- Le temps Delta entre chaque aimation verticale de 'Current'

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
