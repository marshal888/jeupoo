note
	description: "utilisation du protocol r�seau UDP"
	auteur: "Samuel Fortier"
	date: "Dim, 07 Juin 2020 2:20:00"
	revision: "1.0"

class
	RESEAU

create
	make

feature {NONE} -- Initialisation

	make
			-- Ex�cution du programme client
		local
			l_socket: NETWORK_DATAGRAM_SOCKET
			l_port: INTEGER
			l_host: STRING
		do
			l_host := "localhost"
			l_port := 4545
			create l_socket.make_targeted (l_host, l_port)
			create {LINKED_LIST [STRING]} liste_pointage.make
			socket := l_socket
		end

feature {ANY}

	client (a_message: STRING)
			-- Envoi des donn�es au server
		do
			socket.put_integer (a_message.count)
			socket.put_string (a_message)
			socket.close
		end

	receveur_client
			-- Recois les informations du server
		local
			l_socket: NETWORK_DATAGRAM_SOCKET
			l_info_size: INTEGER
			l_information: STRING
		do
			create l_socket.make_bound (4545)
			l_socket.read_integer
			l_info_size := l_socket.last_integer
			l_socket.read_stream (l_info_size)
			l_information := l_socket.last_string
			liste_pointage.extend (l_information)
			l_socket.read_integer
			l_info_size := l_socket.last_integer
			l_socket.read_stream (l_info_size)
			l_information := l_socket.last_string
			liste_pointage.extend (l_information)
			l_socket.close
		end

feature {ANY} -- Impl�mentation

	socket: NETWORK_DATAGRAM_SOCKET

	liste_pointage: LIST [STRING]

invariant

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
