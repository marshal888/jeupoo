note
	description: "Classe parente {ENNEMI}"
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "3.0"

deferred class
	ENNEMI

inherit

	DEPLACABLE
		rename
			make as make_deplacable
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
