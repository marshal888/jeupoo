note
	description: "Classe qui g�re les fonctions d'affichage d'image"
	author: "Samuel Fortier"
	date: "Dim, 07 Juin 2020 18:35:00"
	revision: "4.0"

deferred class
	AFFICHAGE

inherit

	GAME_TEXTURE
		rename
			make as make_texture
		end

feature {NONE} -- Initialisation

	make (a_renderer: GAME_RENDERER; a_file_name: STRING)
			-- Initialisation du `Current' � utiliser avec `a_renderer'

		require
			No_Error: not has_error
		local
			l_image: IMG_IMAGE_FILE
		do
			x := 0
			y := 0
			sub_image_x := 0
			sub_image_y := 0
			has_error := False
			create l_image.make (a_file_name)
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then
					make_from_image (a_renderer, l_image)
					if not has_error then
						sub_image_width := width
						sub_image_height := height
					end
				else
					has_error := True
				end
			else
				has_error := True
			end
		end

feature

	is_mirror: BOOLEAN
			-- Est-ce que 'Current' peut �tre mirroit�

	x: INTEGER assign set_x
			-- Position verticale de `Current'

	y: INTEGER assign set_y
			-- Position horizontale de `Current'

	set_x (a_x: INTEGER)
			-- Assigne la valeur de `x' avec `a_x'
		do
			x := a_x
		ensure
			Is_Assign: x = a_x
		end

	set_y (a_y: INTEGER)
			-- Assigne la valeur de `y' avec `a_y'
		do
			y := a_y
		ensure
			Is_Assign: y = a_y
		end

	sub_image_x, sub_image_y: INTEGER
			-- Position de la portion d'image qui doit �tre montr�e � l'int�rieur de 'surface'

	sub_image_width, sub_image_height: INTEGER
			-- Dimension de la portion d'image qui doit �tre montr�e � l'int�rieur de 'surface'

invariant

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
