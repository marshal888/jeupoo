note
	description: "Classe {BALLE} qui g�re les balles."
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "3.0"

class
	BALLE

inherit

	DEPLACABLE
		rename
			make as make_deplacable
		end

create
	make

feature {NONE}

	make (a_renderer: GAME_RENDERER)
			-- Initialisation de `Current' � utiliser avec `a_renderer'
		do
			movement_delta_horizontal := 1
			make_deplacable (a_renderer, "balle_img.png")
		ensure
			valid_delta: movement_delta_horizontal = 1
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
