note
	description: "Classe du moteur qui sert � utiliser les autres classes."
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "2.0"

class
	ENGINE

inherit

	GAME_LIBRARY_SHARED

	AUDIO_LIBRARY_SHARED

	GAME_RANDOM_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Initialisation de `Current'

		require
			No_Error: not has_error
		local
			l_window_builder: GAME_WINDOW_RENDERED_BUILDER
			--l_music_thread: APP_THREAD
		do
			create l_window_builder
			l_window_builder.set_dimension (1024, 512)
			l_window_builder.set_title ("Doom")
			l_window_builder.enable_must_renderer_synchronize_update
			window := l_window_builder.generate_window
			create hell.make (window.renderer)
			create doomguy.make (window.renderer)
			create {ARRAYED_LIST [ENNEMI]} ennemies.make (2)
			create {LINKED_LIST [BALLE]} bullets.make
			create {ARRAYED_LIST [ITEM]} bonus.make (2)
			ennemies.extend (create {CACODEMON}.make (window.renderer))
			ennemies.extend (create {IMP}.make (window.renderer))
			bonus.extend (create {VIE}.make (window.renderer))
			bonus.extend (create {MUNITION}.make (window.renderer))
			create audio.make ("sound.wav")
			create network.make
			has_error := hell.has_error
			--create l_music_thread.make(agent quit)
			--l_music_thread.launch
			--l_music_thread.join
		end

feature -- Acc�s

	bullets: LIST [BALLE]
			-- `Balle tir�e par l'arme'

	ennemies: LIST [ENNEMI]
			-- Liste d'ennemie

	bonus: LIST [ITEM]
			-- Liste de bonus

	audio: AUDIO
			-- `source audio'

	network: RESEAU
			-- `Connexion � un r�seau'

	run
			-- D�marre le jeu
		require
			No_Error: not has_error
		do
			audio.play_sound (-1)
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (0, 0, 0))
			window.renderer.clear
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (0, 128, 255))
			game_library.quit_signal_actions.extend (agent on_quit)
			window.key_pressed_actions.extend (agent on_key_pressed)
			window.key_released_actions.extend (agent on_key_released)
			game_library.iteration_actions.extend (agent on_iteration)
			if window.renderer.driver.is_present_synchronized_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end
		end

	has_error: BOOLEAN
			-- `True' si il y a une erreur lors de la cr�ation de `Current'

	window: GAME_WINDOW_RENDERED
			-- La fen�tre o� le jeu sera affich�

	doomguy: DOOMGUY
			-- Personnage principale du jeu

	hell: HELL
			-- Le fond d'�cran

feature {NONE} -- Impl�mentation

	doit_quitter: BOOLEAN
			-- La boucle de `make' doit quitter.

	quit
			-- Arr�te la boucle de `make'
		do
			doit_quitter := true
		end

	on_iteration (a_timestamp: NATURAL_32)
			-- �venement qui occure � chaque it�ration du jeu

		local
			l_is_dead: BOOLEAN
		do
			audio_library.update
			doomguy.update (a_timestamp) -- Met � jour l'animation et les coordon�es du Doomguy
			from
				bonus.start
			until
				bonus.exhausted
			loop
				bonus.item.update (a_timestamp)
				if doomguy.x >= bonus.item.x - 10 and doomguy.x <= bonus.item.x + 10 then
					bonus.remove
					doomguy.health_points := 25
					if a_timestamp \\ 2 = 0 then
						bonus.extend (create {MUNITION}.make (window.renderer))
					else
						bonus.extend (create {VIE}.make (window.renderer))
					end
				else
					bonus.forth
				end
			end
			from
				ennemies.start
			until
				ennemies.exhausted
			loop
				ennemies.item.update (a_timestamp)
				random.generate_new_random
				ennemies.item.y := ennemies.item.y + random.last_random_integer_between (-3, 3)
				l_is_dead := false		-- Si l'ennemi est mort ou non
				from
					bullets.start
				until
					bullets.exhausted or l_is_dead
				loop
					if bullets.item.x >= ennemies.item.x and bullets.item.x < ennemies.item.x + 15 and bullets.item.y >= ennemies.item.y - 25 and bullets.item.y < ennemies.item.y + 55 then
						l_is_dead := true
					end
					bullets.forth
				end
				if l_is_dead then
						--reseau.client ("1")
					ennemies.remove
					if a_timestamp \\ 2 = 0 then
						ennemies.extend (create {CACODEMON}.make (window.renderer))
					else
						ennemies.extend (create {IMP}.make (window.renderer))
					end
				else
					ennemies.forth
				end
			end
			across
				bullets as la_bullets
			loop
				la_bullets.item.update (a_timestamp)
			end
			if doomguy.x < 0 then -- Be sure that Doomguy does not get out of the screen
				doomguy.x := 0
			elseif doomguy.x + doomguy.sub_image_width > hell.width then
				doomguy.x := hell.width - doomguy.sub_image_width
			end

				-- Dessine le fond
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (0, 128, 255))
			window.renderer.draw_filled_rectangle (0, 0, hell.width, hell.height)
			draw_affichable (hell)
			draw_affichable (doomguy)
			across
				bullets as la_bullets
			loop
				draw_affichable (la_bullets.item)
			end
			across
				bonus as la_bonus
			loop
				draw_affichable (la_bonus.item)
			end
			across
				ennemies as la_ennemies
			loop
				draw_affichable (la_ennemies.item)
				if la_ennemies.item.y < 200 then
					la_ennemies.item.y := 200
				elseif la_ennemies.item.y > 375 then
					la_ennemies.item.y := 375
				end
			end
			window.renderer.present -- Met � jour les modifications dans l'�cran
		end

	draw_affichable (affichage: AFFICHAGE)
			-- Affiche les images
		do
			window.renderer.draw_sub_texture_with_mirror (affichage, affichage.sub_image_x, affichage.sub_image_y, affichage.sub_image_width, affichage.sub_image_height, affichage.x, affichage.y, False, affichage.is_mirror)
		end

	on_key_pressed (a_timestamp: NATURAL_32; a_key_event: GAME_KEY_EVENT)
			-- Les actions qui se produiront quand une touche est appuy�e
		do
			if not a_key_event.is_repeat then
				if a_key_event.is_right then
					doomguy.go_right (a_timestamp)
				elseif a_key_event.is_left then
					doomguy.go_left (a_timestamp)
				elseif a_key_event.is_up then
					doomguy.jump (a_timestamp)
				elseif a_key_event.is_space then
					bullets.extend (create {BALLE}.make (window.renderer))
					bullets.last.x := doomguy.x + 50
					bullets.last.y := doomguy.y + 15
					bullets.last.go_right (a_timestamp)
					doomguy.fire
				elseif a_key_event.is_escape then --QUIT
					on_quit (a_timestamp)
				end
			end
		end

	on_key_released (a_timestamp: NATURAL_32; a_key_event: GAME_KEY_EVENT)
			-- Les actions qui se produiront quand une touche sera rel�ch�e
		do
			if not a_key_event.is_repeat then
				if a_key_event.is_right then
					doomguy.stop_right
				end
				if a_key_event.is_left then
					doomguy.stop_left
				end
			end
		end

	on_quit (a_timestamp: NATURAL_32)
			-- Quand on quitte le programme, cette fonction est lanc�e et arr�te les librairies.
		do
			game_library.stop
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
