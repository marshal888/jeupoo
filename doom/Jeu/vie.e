note
	description: "Classe {VIE} qui g�re les fonctions des vies"
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "1.0"

class
	VIE

inherit

	ITEM

	GAME_RANDOM_SHARED

create
	make

feature

	make (a_renderer: GAME_RENDERER)
			-- Initialisation de `Current' � utiliser avec `a_renderer'
		do
			make_deplacable (a_renderer, "healthpack.png")
			random.generate_new_random
			x := random.last_random_integer_between (10, 500)
			y := 395
		ensure
			valid_y: y=395
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
