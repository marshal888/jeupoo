note
	description: "Le fond d'�cran du jeu."
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "2.0"

class
	HELL

inherit

	AFFICHAGE
		rename
			make as make_affichage
		end

create
	make

feature -- Initialisation

	make (a_renderer: GAME_RENDERER)
			-- Initialisation de `Current' � utiliser avec `a_renderer'

		require
			No_Error: not has_error
		do
			make_affichage (a_renderer, "desert.png")
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
