note
	description: "Root class for the animation using renderer example"
	author: "Samuel Fortier"
	date: "Mer, 5 mai 2020 18:35:00 "
	revision: "2.0"

class
	APPLICATION

inherit

	GAME_LIBRARY_SHARED

	IMG_LIBRARY_SHARED

	AUDIO_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Execute l'application.

		local
			l_engine: ENGINE
		do
			audio_library.enable_playback -- Rend disponible les fonctions Audio
			game_library.enable_video -- Rend disponible les fonctions Vid�o
			image_file_library.enable_image (true, false, false) -- Rend disponible les PNG (mais pas TIF ou JPG).
			create l_engine.make
			if not l_engine.has_error then
				l_engine.run
			end
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"

end
