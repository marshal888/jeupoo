note
	description: "G�re les fonctions audio."
	author: "Samuel Fortier"
	date: "Dim, 07 juin 2020 18:35:00"
	revision: "3.0"

class
	AUDIO

inherit

	AUDIO_LIBRARY_SHARED

create
	make

feature -- Initialisation

	make (a_sound_file: STRING)
			-- Execute l'application.
		do
			audio_library.sources_add -- Ajoute une source de son dans le contexte audio
			source := audio_library.last_source_added
			create sound.make (a_sound_file)
			sound.open
		end

	source: AUDIO_SOURCE
			-- Source audio

	sound: AUDIO_SOUND_WAV_FILE
			-- Le son � jouer

	play_sound (a_repetition: INTEGER)
			-- Jouer le son d�sir�
		require
			sound_is_open: source.is_open
		do
			source.stop
			source.queue_sound_loop (sound, a_repetition)
			source.play
		ensure
			source_open: source.is_open
		end

note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
