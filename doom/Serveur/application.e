note
	description: "Le serveur"
	date: "Ven, 9 mai 2020 2:25:00"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

make
			-- Ex�cution du serveur qui recois des donn�es du jeu
		local
			l_socket: NETWORK_DATAGRAM_SOCKET
			l_port: INTEGER
			l_taille_message: INTEGER
			l_message: STRING
			l_score : INTEGER
		do
			l_port := 4545
			create l_socket.make_bound (l_port)
			l_socket.read_integer
			l_score := l_socket.last_integer
			io.put_string ("Highscore : ")
			io.put_integer (l_score)

end
note
	copyright: "Tous droits r�serv�s (c) 2020, Samuel Fortier"
	license: "GNU General Public License v3.0"
	source: "C�gep de Drummondville"
end
