Version : 4.0
Date : 07 juin 2020
Auteur : Samuel Fortier

Compilation :

	Sur Windows
 Installer EiffelStudio :
 Télécharger EiffelStudio à l'adresse: https://www.eiffel.org/
 Installer EiffelStudio
 Installer Eiffel Game 2:
 Télécharger Eiffel_Game2.zip:
https://github.com/tioui/Eiffel_Game2/tree/windows_build
 Copier le répertoire "game2" dans le sous-répertoire d'EiffelStudio contrib/library/
 Copier les fichiers dll du fichier zip correspondant à votre version d'Eiffelstudio (DLL32.zip ou DLL64.zip) dans le répertoire de votre projet (le même répertoire que votre fichier .ecf). 

	Sur Linux
1. Installez EiffelStudio en entrant les commandes suivantes :
# sudo add-apt-repository ppa:eiffelstudio-team/ppa
# sudo apt-get update
# sudo apt-get install eiffelstudio
2. Installez les librairies C :
# sudo apt install git libopenal-dev libsndfile1-dev libgles2-mesa-dev
# sudo apt install libsdl2-dev libsdl2-gfx-dev libsdl2-image-dev libsdl2-ttf-dev
# sudo apt install libepoxy-dev libgl1-mesa-dev libglu1-mesa-dev
3. Télécharger Eiffel Game 2
# git clone --recursive https://github.com/tioui/Eiffel_Game2.git
4. Créer un lien entre le répertoire EiffelStudio et Eiffel_Game2 :
# sudo ln -s `pwd`/Eiffel_Game2 /usr/lib/eiffelstudio-18.11/contrib/library/game2
Si cette commande vous donne une erreur de type chemin introuvable, vérifiez la version
d’EiffelStudio installé sur votre système avec la commande suivant et adaptez la commande précédente en changeant le numéro de version.
# ls /usr/lib/ | grep eiffelstudio
5. Compiler la librairie :
# cd Eiffel_Game2
# ./compile_c_library.sh

Ouvrir le fichier doom/serveur/project.ecf
Compiler et exécuter project.ecf dans EiffelStudio pour lancer le serveur
Ouvrir le fichier doom/jeu/doom.ecf
Compiler et exécuter doom.ecf dans EiffelStudio
Utilisez les 4 flèches du clavier pour bouger de gauche à droit et vers le haut. Appuyez sur « Espace » pour tirer un projectile.
Deux ennemis apparaîtront dans la partie droite de l’écran en bougeant de manière erratique. Si un projectile les atteints, un nouvel ennemi apparaîtra.

